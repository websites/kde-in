<?php
/**
 * The Footer widget areas.
 *
 */
?>

<?php
	/* The footer widget area is triggered if any of the areas
	 * have widgets. So let's check that first.
	 *
	 * If none of the sidebars have widgets, then let's bail early.
	 */
	if (   ! is_active_sidebar( 'first-footer-widget-area'  )
		&& ! is_active_sidebar( 'second-footer-widget-area' )
		&& ! is_active_sidebar( 'third-footer-widget-area'  )
	)
		return;
	// If we get this far, we have widgets. Let do this.
?>

			<ul class="widgets">

<?php if ( is_active_sidebar( 'first-footer-widget-area' ) ) : ?>
				<li id="first" class="widget">
					<ul class="widgets">
						<?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
					</ul>
				</li><!-- #first .widget-area -->
<?php endif; ?>

<?php if ( is_active_sidebar( 'second-footer-widget-area' ) ) : ?>
				<li id="second" class="widget">
					<ul class="widgets">
						<?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
					</ul>
				</li><!-- #second .widget-area -->
<?php endif; ?>

<?php if ( is_active_sidebar( 'third-footer-widget-area' ) ) : ?>
				<li id="third" class="widget">
					<ul class="widgets">
						<?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
					</ul>
				</li><!-- #third .widget-area -->
<?php endif; ?>

			</ul><!-- #footer-widget-area -->
