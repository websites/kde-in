<?php
/**
 * The template for displaying the footer.
 */
?>
	</div><!-- #main -->

	<div id="footer">
		<div class="content">

<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>
		<div class="clear"></div>
		</div> <!-- content-->

			<div id="copyright">
				&copy; KDE India 2010-2011
				&bull;
				Theme by
				<a href="<?php echo esc_url( __('http://likalo.com/', true) ); ?>" rel="nofollow">Eugene Trounev</a>
			</div><!-- #copyright -->

	</div><!-- #footer -->
   </div> <!-- page-->
</div> <!-- wrapper-->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</body>
</html>
