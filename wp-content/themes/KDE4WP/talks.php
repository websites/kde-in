<?php
/**
 * Template Name: Talks Page
 *
 * Sayak's tiny little template page.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 */
update_option('show_conf_menu',true);
get_header(); 

$link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
	mysql_select_db(DB_NAME, $link);
	mysql_set_charset(DB_CHARSET, $link);
?>

<?php get_sidebar(); ?>
    <div class="content">
    <h1>Talks</h1>
    
    <?php if (!isset($_GET['id'])) { ?>
    
	<p style="margin-left:10px">
	We got many good talks and tutorial submissions. 
	Thank you to all those who submitted such wonderful talks. 
	We have finalized the following talks / tutorials for conf.kde.in/2011.
	Click on a talk from the following list to view detailed information.</p>
	
	<ol>
	    <?php 
		$talks = array();
		$count = 0;
		$result = mysql_query("SELECT sub_id, field_val FROM wp_cformsdata WHERE field_name='Talk Title'");
		
		while ($row = mysql_fetch_assoc($result))
		{
		    $talks["id"][$count] = $row['sub_id'];
		    $talks["title"][$count] = $row['field_val'];
		    $count++;
		}
		
		$result = mysql_query("SELECT sub_id, field_val FROM wp_cformsdata WHERE field_name='Name' ORDER BY field_val ASC");
		
		while ($row = mysql_fetch_assoc($result))
		{
		    for ($i = 0; $i < $count; $i++)
		    {
			if ($talks['id'][$i] == $row['sub_id'])
			{
			    echo '<li><a href="?id=' . $talks['id'][$i] . '">' . $row['field_val'] .
			    ' - <i>' . $talks['title'][$i] . '</i></a></li>';
			}
		    }
		}
	    ?>
	</ol>
	
	<p style="margin-left:10px">
	Note that a few submissions have been held back till we receive pending 
	clarification from the respective speakers. We will get in touch with 
	those speakers and update this list accordingly. Thank you for understanding.
	</p>
	
    <?php } else { 
    
	$id = $_GET['id'];
	$result = mysql_query("SELECT field_name, field_val FROM wp_cformsdata WHERE sub_id={$id}");
	$data = array();
	
	if (mysql_num_rows($result) == 0)
	{
	    echo "<p style=\"margin-left:10px\">Whoopsie! That talk ID is invalid. Please see " .
		 "<a href=\"http://conf.kde.in/talks\">http://conf.kde.in/talks</a>" .
		 " for a complete list of talks.</p>";
	}
	
		
	while ($row = mysql_fetch_assoc($result))   
	{
	    $data[$row['field_name']] = nl2br($row['field_val']);
	} ?>
	
	<a href="http://kde.in/conf/schedule/">&laquo; Return to the talk list</a><br /><br />
    
	<table>
	    <tr>
	    <th colspan="2">Speaker Details</th>
	    </tr>
	    
	    <tr>
		<td style="vertical-align:top;">Name</td>
		<td><?php echo $data['Name'] ?></td>
	    </tr>
	    
	    <tr>
		<td style="vertical-align:top;">Website</td>
		<td><a href="<?php echo $data['Website'] ?>" rel="nofollow"><?php echo $data['Website'] ?></a></td>
	    </tr>
	    
	    <tr>
		<td style="vertical-align:top;">Bio</td>
		<td><?php echo $data['Bio'] ?></td>
	    </tr>
	</table>
	    
	<table>
	    <tr>
	    <th colspan="2">Talk Details</th>
	    </tr>
	    
	    <tr>
		<td style="vertical-align:top;">Talk Title</td>
		<td><?php echo $data['Talk Title'] ?></td>
	    </tr>
	    
	    <tr>
		<td style="vertical-align:top;">Abstract</td>
		<td><?php echo $data['Abstract'] ?></td>
	    </tr>
	    
   	    <tr>
		<td style="vertical-align:top;">Session Type</td>
		<td><?php echo $data['Session Type'] ?></td>
	    </tr>

	    <tr>
		<td style="vertical-align:top;">Difficulty</td>
		<td><?php echo $data['Difficulty Level'] ?></td>
	    </tr>
	    
	    <tr>
		<td style="vertical-align:top;">Prerequisites</td>
		<td><?php echo empty($data['Prerequisites']) ? "None" : $data['Prerequisites'] ?></td>
	    </tr>

	</table>
	
	<a href="http://conf.kde.in/talks">&laquo; Return to the talk list</a>
    
    <?php } ?>
	
    <div class="clear"></div>
    </div> <!-- content-->

<?php get_footer(); ?>
