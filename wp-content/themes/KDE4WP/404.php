<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<?php get_sidebar(); ?>
		<div class="content">

			<div id="post-0" class="post error404 not-found">
				<h1 class="entry-title"><?php _e( 'Oops!', 'twentyten' ); ?></h1>
				<div class="centered">
				    <img src="<?php bloginfo('template_directory'); ?>/images/emote.png" alt="" />
				</div>
				<div class="entry-content">
					<p>
					    Now this is embarrassing! We couldn't find the page you requested for.
					    If you followed a link somewhere, you can let us know by sending a message
					    using our <a href="http://kde.in/contact/">contact page</a>.
					    <br />Thanks! :)
					</p>
				</div><!-- .entry-content -->
			</div><!-- #post-0 -->

		<div class="clear"></div>
		</div> <!-- content-->

	<script type="text/javascript">
		// focus on search field after it has loaded
		document.getElementById('s') && document.getElementById('s').focus();
	</script>

<?php get_footer(); ?>