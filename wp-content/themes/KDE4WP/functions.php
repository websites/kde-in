<?php
/**
 * KDE4WP functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, kde4wp_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 */

/** Tell WordPress to run kde4wp_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', 'kde4wp_setup' );

if ( ! function_exists( 'kde4wp_setup' ) ):

function kde4wp_setup() {

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'main' => __( 'Main Menu', 'kde4wp' ),
		'conf' => __( 'Conference Menu', 'kde4wp' ),
	) );
}
endif;

function kde4wp_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'kde4wp_page_menu_args' );


/**
 * Register widgetized areas.
 *
 */
function kde4wp_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'kde4wp' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The primary widget area', 'kde4wp' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Secondary Widget Area', 'kde4wp' ),
		'id' => 'secondary-widget-area',
		'description' => __( 'The secondary widget area', 'kde4wp' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 3, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'First Footer Widget Area', 'kde4wp' ),
		'id' => 'first-footer-widget-area',
		'description' => __( 'The first footer widget area', 'kde4wp' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 4, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Second Footer Widget Area', 'kde4wp' ),
		'id' => 'second-footer-widget-area',
		'description' => __( 'The second footer widget area', 'kde4wp' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 5, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Third Footer Widget Area', 'kde4wp' ),
		'id' => 'third-footer-widget-area',
		'description' => __( 'The third footer widget area', 'kde4wp' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

}
/** Register sidebars by running kde4wp_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'kde4wp_widgets_init' );
